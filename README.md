# pyacmegraph
[ACME](http://baylibre.com/acme) power probe capture and analysis tool

![pageres](media/pyacmegraph-sample01.png)

**Major features:**
 - auto-detects connected probes and Rshunt, captures data
 - computes and displays Power consumption and Vbat with multiple zoom capabilities
 - basic stats (mean on Power, Vbat, distribution graph)
 - save captured data for later visualization (view mode), save as text and image
 - control ACME power switch

**Please submit bugs or feature requests using this project [Issues](https://gitlab.com/baylibre-acme/pyacmegraph/issues) tracking.**

# Usage
## Examples
```
pyacmegraph.py --ip <ip address or network name of the ACME board>

pyacmegraph.py --load capture.acme
   => starts in display mode (no capture), loads and displays data from capture.acme file

pyacmegraph.py --ip myacme.local --template capture.acme
   => Captures data, but initializes parameters from capture.acme file
```

## Options
```
usage: pyacmegraph.py [-h] [--load file] [--template file] [--inttime [value]]
                      [--oversmplrt value] [--norelatime] [--ip IP]
                      [--shunts SHUNTS] [--vbat VBAT] [--ishunt]
                      [--forcevshuntscale [scale]] [--timeoffset time]
                      [--verbose]

ACME measurements capture and display tool.

optional arguments:
  -h, --help          show this help message and exit
  --load file         load .acme file containing data to display (switches to
                      display-only mode)
  --template file     load .acme file settings section only (colors, plot
                      names, shunts ...). Useful for starting a fresh capture
                      session re-using a previous saved setup
  --inttime [value]   integration time to use instead of default value
                      (0.000588s). Use without value to get the list of
                      accepted values
  --oversmplrt value  oversampling ratio to use instead of default value (1)
  --norelatime        display absolute time from device
  --ip IP             IP address of ACME
  --shunts SHUNTS     list of shunts to use in mOhms (comma separated list,
                      one shunt value per channel, starting at channel 0) Ex:
                      100,50,250
  --meancapture         Capture mean values only (compute the mean values of
                        the capture buffers and store only these mean values).
                        Full data are dropped. Helps with long captures.
  --vbat VBAT         Force a constant Vbat value (in Volts) to be used for
                      computing power, in place of ACME measured vbat
  --ishunt            Display Ishunt instead of Power (do not capture Vbat and
                      do not compute Power
  --forcevshuntscale [scale]
                      Override Vshunt scale value, and force application
                      start even if identifying a Vshunt scaling problem
  --timeoffset time   Add an offset to displayed time (can be negative) in
                      offline mode
  --verbose, -v       print debug traces (various levels v, vv, vvv)

This tools captures exclusively Vbat and Vshunt values from ACME probes. Using Rshunt
(auto-detected or forced), it computes and displays the resulting power (Vbat*Vshunt/Rshunt).
Capture settings are automatically setup to optimize sampling resolution, but can be overriden.
Example usage:
./pyacmegraph.py --ip baylibre-acme.local --shunts=100,50,250 -v
./pyacmegraph.py --load capture.acme --timeoffset -10253
```
## Details
For more details about the pyacmegraph tool please refer to : 

[acme-pyacmegraph-part-1-2](https://baylibre.com/acme-pyacmegraph-part-1-2/)

[acme-and-pyacmegraph-part-2-2](https://baylibre.com/acme-and-pyacmegraph-part-2-2/)


## Getting Started

### Prerequisites
 - Python 2.7
 - ACME board running a recent distro with iio
 See here for latest IIO capable SW release:
 - [pre-built image with IIO](https://gitlab.com/baylibre-acme/ACME-Software-Release)
or
 - [building the SW with IIO](https://gitlab.com/baylibre-acme/ACME)

### Installing on Linux

Install Python 3 and following python extensions: numpy, pyqt4, pyqtgraph, libiio, python-libiio. Ex.:
```
sudo apt-get install python-numpy python-qt4 python-pyqtgraph libiio python-libiio
```
(for pyqtgraph debian package, you may need to install the related .deb from [pyqtgraph](http://www.pyqtgraph.org) website, or add the project-specific Debian repository)


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
